package app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        System.out.println("I want to play a little game...");
        System.out.println(" ");
        System.out.println("Здравствуй, Олег. Я хочу сыграть в игру.");
        System.out.println("Выбери сложность от 1 до 3.");
        Scanner input = new Scanner(System.in);
        int level = input.nextInt();
        int k = 5;
            switch (level) {
                case 1:
                    System.out.println("Ты выбрал минимальный уровень сложности");
                    System.out.println("Задача: угадать следующее число. У тебя есть три попытки ");
                    for (int i = 0; i < 3; i++) {
                        k -= 2;
                        System.out.print(k);
                        System.out.print(" ");
                        k *= 2;
                        System.out.print(k);
                        System.out.print(" ");
                    }
                    int attempt = 0;
                    while (attempt < 3) {
                        System.out.println("Вводи число");
                        int num = input.nextInt();
                        if (num != 10) {
                            attempt++;
                            System.out.println("Неверно");
                            System.out.println("Осталось попыток " + (3 - attempt));
                        } else {
                            System.out.println("Ты смог");
                            break;
                        }
                    }
                    System.out.println("Правильный ответ был 10");
                    break;


                case 2:
                    System.out.println("Ты выбрал средний уровень сложности");
                    System.out.println("Задача: угадать следующее число. У тебя есть три попытки ");
                    for (int i = 0; i < 3; i++) {
                        k += 4;
                        System.out.print(k);
                        System.out.print(" ");
                        k *= 2;
                        System.out.print(k);
                        System.out.print(" ");
                        k -= 3;
                        System.out.print(k);
                        System.out.print(" ");
                    }
                    int attempt2 = 0;
                    while (attempt2 < 3) {
                        System.out.println("Вводи число");
                        int num = input.nextInt();
                        if (num != 79) {
                            attempt2++;
                            System.out.println("Неверно");
                            System.out.println("Осталось попыток " + (3 - attempt2));
                        } else {
                            System.out.println("Ты смог");
                            break;
                        }
                    }
                    System.out.println("Правильный ответ был 79"); break;

                case 3:
                    System.out.println("Ты выбрал максимальный уровень сложности");
                    System.out.println("Задача: угадать следующее число. У тебя есть три попытки ");
                    for (int i = 0; i < 3; i++) {
                        k *= 2;
                        System.out.print(k);
                        System.out.print(" ");
                        k += 8;
                        System.out.print(k);
                        System.out.print(" ");
                        k /= 2;
                        System.out.print(k);
                        System.out.print(" ");
                        k -= 5;
                        System.out.print(k);
                        System.out.print(" ");
                    }
                    int attempt3 = 0;
                    while (attempt3 < 3) {
                        System.out.println("Вводи число");
                        int num = input.nextInt();
                        if (num != 4) {
                            attempt3++;
                            System.out.println("Неверно");
                            System.out.println("Осталось попыток " + (3 - attempt3));
                        } else {
                            System.out.println("Ты смог");
                            break;
                        }
                    }
                    System.out.println("Правильный ответ был 4"); break;
                default:
                    System.out.println("Ты упустил свой шанс");
            }



    }
}


